<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard()
    {
        return view('home');
    }

    public function table()
    {
        return view('table');
    }

    public function dataTables()
    {
        return view('data-tables');
    }
}
